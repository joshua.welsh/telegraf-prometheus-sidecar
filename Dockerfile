FROM telegraf:1.10

COPY telegraf.conf /etc/telegraf/telegraf.conf

EXPOSE 9273/TCP
EXPOSE 8094/UDP
EXPOSE 8125/UDP
